# README #

### What is this repository for? ###
This is a python program that compares three different Machine Learning Classification Algorithms based on how they classify salary Estimates from the UCI Adult dataset(https://archive.ics.uci.edu/ml/datasets/adult).

### How do I get set up? ###

1) Download the data set from https://archive.ics.uci.edu/ml/datasets/adult.
2) Place it in the same folder as the python program.
3) Run the Program using Anaconda Jupyter Notebook



### Who do I talk to? ###

Email: aditya91pv@gmail.com